/*
PF2E Character Adjustments

Used for applying Weak and Elite adjustments to NPCs

Version     0.1
Author:     Mark Stoecker
Roll20:     https://app.roll20.net/users/580967/mark-s
BitBucket:  https://bitbucket.org/desertwebdesigns/roll20/src/master/PF2Adjustments/

*/
var RLRGaming = RLRGaming || {};

RLRGaming.Adjustments = RLRGaming.Adjustments || (() => {
    'use strict';
    
    const version = "0.1";

    const getCharByToken = (tokenid) => {
        var token = getObj("graphic", tokenid);
        if (token === undefined)
            return false;
        else
            return getObj("character", token.get('represents'));
    };
    
    const weakHP = (character) => {
        var level = getAttrByName(character.id, 'level');
        var hpAdj = 0;
        switch (true) {
            case (level >= 1 && level <= 2):
                hpAdj = -10
                break;
            
            case (level >= 3 && level <= 5):
                hpAdj = -15
                break;
            
            case (level >=6 && level <= 20):
                hpAdj = -20
                break;
            
            case (level >= 21):
                hpAdj = -30
                break;
        };
        return hpAdj;
    }

    const eliteHP = (character) => {
        var level = getAttrByName(character.id, 'level');
        var hpAdj = 0;
        switch (true) {
            case (level <= 1):
                hpAdj = 10
                break;
            
            case (level >= 2 && level <= 4):
                hpAdj = 15
                break;
            
            case (level >=5 && level <= 19):
                hpAdj = 20
                break;
            
            case (level >= 20):
                hpAdj = 30
                break;
        };
        return hpAdj;
    }
    
    const adjust = (type, token) => {
        // Get Character
        var character = getCharByToken(token);
        if (!character)
            return false;

        switch (type) {
            case 'weak':
                var adjustment = -2;
                var hpAdj = weakHP(character);
                break;
            case 'elite':
                var adjustment = 2;
                var hpAdj = eliteHP(character);
                break;
        };
        
        // Adjust HP
        var hp = findObjs({type: 'attribute', characterid: character.id, name: 'hit_points'})[0];
        hp.set('current', Number(hp.get('current'))+hpAdj);
        hp.set('max', Number(hp.get('max'))+hpAdj);

        // Adjust AC
        var ac = findObjs({type: 'attribute', characterid: character.id, name: 'armor_class'})[0];
        ac.set('current', Number(ac.get('current'))+adjustment);

        // Adjust Skill Modifiers, Saving Throws, and Weapon Strike Modifiers
        var toModify = ['acrobatics','arcana','athletics','crafting','deception','diplomacy','intimidation','lore','medicine','nature','occultism','performance','religion','society','stealth','survival','thievery','saving_throws_fortitude','saving_throws_reflex','saving_throws_will'];
        
        if (type == 'elite')
            toModify.push('perception');

        _.chain(findObjs({type: 'attribute', characterid: character.id}))
            .map((a) => {
                if ((toModify.includes(a.get('name'))) || (/_weapon_strike$/.test(a.get('name')))) {
                    return a;
                }
            })
            .filter((o) => o)
            .each((o) => o.set('current', Number(o.get('current'))+adjustment));

        // Adjust Weapon Strike Damage
         _.chain(findObjs({type: 'attribute', characterid: character.id}))
            .map((a) => {
                if (/_weapon_strike_damage$/.test(a.get('name'))) {
                    return a;
                }
            })
            .filter((o) => o)
            .map((a) => {
                var mod = 0, modstr = '';
                var match = a.get('current').match(/^(\d+d\d+)([+-]?)?(\d+)?$/);
                if (match) {
                    switch(match[2]) {
                        case '+':
                            mod = Number(match[3])+adjustment;
                            switch(Math.sign(mod)) {
                                case -1:
                                    modstr = String(mod);
                                    break;
                                case 1:
                                    modstr = "+" + String(mod);
                                    break;
                            }
                            break;
                        case '-':
                            mod = Number("-" + match[3])+adjustment;
                            modstr = String(mod);
                            break;
                        case undefined:
                            modstr = (type == 'weak') ? String(adjustment) : String("+" + adjustment);
                            break;
                    }
                }
                a.set('current', match[1] + modstr);
            });
    };

    const handleInput = async (msg) => {
        if(msg.type == "api" && playerIsGM(msg.playerid)) {
            var args = msg.content.split(" ");
            switch(args[0].toLowerCase()) {
                case '!weak':
                    if (args.length == 2) {
                        adjust("weak", args[1]);
                    } else {
                        sendChat("GM", "/w GM Incorrect number of parameters sent to '!weak'");
                        return;
                    }
                    break;
                case '!elite':
                    if (args.length == 2) {
                        adjust("elite", args[1]);
                    } else {
                        sendChat("GM", "/w GM Incorrect number of parameters sent to '!elite'");
                        return;
                    }
                    break;
            }
        }
    };
    
    const registerEventHandlers = async () => {
        on('chat:message', handleInput);
    };
    
    return {
        RegisterEventHandlers: registerEventHandlers
    };
})();


on('ready', async () => {
    'use strict';
    RLRGaming.Adjustments.RegisterEventHandlers();
});

