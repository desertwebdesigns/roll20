/*
Golarion Calendar

API Commands:
!cal (as Player) - Shows date, time, weather and counted days 
!cal (as GM) - Same as player but includes options to advance the date/time, alter the weather, add a note or view the settings menu.
!calSet (GM only) - Allows the GM to change the date, time display, adjust day counter or set the start date
!addtime (GM only) - Adds hours and minutes to the current time
!useimage (GM only) - Binary, Sets whether to use rollable table images for setting time
!setimage (GM only) - Sets a numerical side of the rollable table as the current time

Red Colour: #7E2D40
*/

var Calendar = Calendar || (function() {
    'use strict';
    
    const version = "6.3";
    
    var handoutName = 'Events Log',
    
    setDefaults = function() {
        state.Calendar = {
            now: {
                version: version,
                ordinal: 1,
                year: 4720,
                down: 0,
                divider: 0,
                weather: "It is a cool but sunny day.",
                timetype: "OFF",
                time: "Noon",
                startdate: "1,Abadius,4720",
                events: "",
                useimage: "OFF",
                clockimage: "",
            },
        };
    },
    
    checkDefaults = function() {
        if( state.Calendar.now.version != version ){
            state.Calendar.now.version = version;
        }
        if( ! state.Calendar.now.ordinal){state.Calendar.now.ordinal = 1};
        if( ! state.Calendar.now.year){state.Calendar.now.year = 4720};
        if( ! state.Calendar.now.down){state.Calendar.now.down = '0'};
        if( ! state.Calendar.now.divider){state.Calendar.now.divider = '0'};
        if( ! state.Calendar.now.weather){state.Calendar.now.weather = "It is a cool but sunny day."};
        if( ! state.Calendar.now.timetype){state.Calendar.now.timetype = "OFF"};
        if( ! state.Calendar.now.time){state.Calendar.now.time = "Noon"};
        if( ! state.Calendar.now.startdate){state.Calendar.now.startdate = "1,Abadius,4720"};
        if( ! state.Calendar.now.events){state.Calendar.now.events = ""};
        if( ! state.Calendar.now.useimage){state.Calendar.now.useimage = "OFF"};
        if( ! state.Calendar.now.clockimage){state.Calendar.now.clockimage = ""};
    },
    
    handleInput = function(msg) {
        var args = msg.content.split(",");

        if (msg.type !== "api") {
			return;
		}

		if(playerIsGM(msg.playerid)){
		    switch(args[0]) {
		        case '!cal':
                    calmain();
                    break;
                case '!settings':
                    calmenu();
                    break;
                case '!startdate':
                    state.Calendar.now.startdate=args[1]+','+args[2]+','+args[3];
                    calmenu();
                    break;
                case '!setday':
                    getOrdinal(msg);
                    weather();
                    calmenu();
                    break;
                case '!setmonth':
                    getOrdinal(msg);
                    weather();
                    calmenu();
                    break;
                case '!setyear':
                    state.Calendar.now.year=args[1];
                    calmenu();
                    break;
                case '!setordinal':
                    state.Calendar.now.ordinal=args[1];
                    calmenu();
                    break;
                case '!useimage':
                    state.Calendar.now.useimage=args[1];
                    if (state.Calendar.now.useimage == "OFF") {
                        state.Calendar.now.clockimage = "";
                    } else {
                        state.Calendar.now.clockimage = args[2];
                    }
                    calmenu();
                    break;
                case '!setimage':
                    if (state.Calendar.now.useimage === "OFF"){
                        state.Calendar.now.clockimage = "";
                        sendChat("Calendar","/w gm You must change the calendar settings to use images.");
                        return;
                    }
                    if (Number.isInteger(parseInt(args[1])) === false) {
                        sendChat("Calendar","/w gm The selected side was not a number.");
                        return;
                    }
                    setimage(state.Calendar.now.clockimage, parseInt(args[1]));
                    break;
                case '!settimetype':
                    var type = args[1];
                    state.Calendar.now.timetype=type; 
                    if (type == 'OFF') { state.Calendar.now.time = 'OFF' };
                    if (type == 'General') { state.Calendar.now.time = 'Morning (dawn)' };
                    if (type == '24 Hour') { state.Calendar.now.time = '00:00' };
                    calmenu();
                    break;
                case '!settime':
                    var type = state.Calendar.now.timetype;
                    if (type == 'General'){
                        state.Calendar.now.time = args[1];
                    } else if (type == '24 Hour') {
                        state.Calendar.now.time = args[1]+args[2];
                        var clockface = (Number(args[1]) >= 12) ? Number(args[1]) - 12 : Number(args[1]);
                        setimage(state.Calendar.now.clockimage, clockface);
                    }
                    calmain();
                    break;
                case '!setdown':
                    var down = Number(args[1]);
                    state.Calendar.now.down = down;
                    getdown(down);
                    calmain();
                    break;
                case '!setdiv':
                    state.Calendar.now.divider=Number(args[1]);
                    calmenu();
                    break;
                case '!addday':
                    addday(args[1]);
                    weather();
                    break;
                case '!addtime':
                    addtime(args[1], args[2]);
                    break;
                case '!weather':
                    if(args[1]=='Roll}'){
                        weather();
                    }else{
                        var string = args[1];
                        for (var i = 2; i < args.length; i++) {
                            string = string + ", " + args[i];
                        }
                        state.Calendar.now.weather = string;
                    }
                    calmain();
                    break;
                case '!log':
                    eventlog(msg);
                    break;    
                case '!playercal':
                    showcal(msg);
                    break;
    	    }
		}else if(args[0]=='!cal'){
		    showcal(msg);
		}
    },
    
    calstyle = function() {
        var colour = '#7E2D40';
        var divstyle = 'style="width: 189px; border: 1px solid black; background-color: #ffffff; padding: 5px; font-size: 12px;"'
        var astyle1 = 'style="text-align:center; border: 1px solid black; margin: 1px; padding: 2px; background-color: ' + colour + '; border-radius: 4px;  box-shadow: 1px 1px 1px #707070; width: 100px;';
        var astyle2 = 'style="text-align:center; border: 1px solid black; margin: 1px; padding: 2px; background-color: ' + colour + '; border-radius: 4px;  box-shadow: 1px 1px 1px #707070; width: 150px;';
        var tablestyle = 'style="border: none;"';
        var arrowstyle = 'style="border: none; border-top: 3px solid transparent; border-bottom: 3px solid transparent; border-left: 195px solid ' + colour + '; margin-bottom: 2px; margin-top: 2px;"';
        var headstyle = 'style="color: ' + colour + '; font-size: 18px; text-align: left; font-variant: small-caps; font-family: Times, serif;"';
        var substyle = 'style="font-size: 11px; line-height: 13px; margin-top: -3px; font-style: italic;"';
        var tdstyle = 'style="padding: 2px; padding-left: 5px; border: none;"';
        
        var styles = colour + '|' + divstyle + '|' + astyle1 + '|' + astyle2 + '|' + tablestyle + '|' + arrowstyle + '|' + headstyle + '|' + substyle + '|' + tdstyle;
        return styles;
    },
    
    calmain = function() {
        var styles = calstyle().split("|");
        var colour = styles[0];
        var divstyle = styles[1];
        var astyle1 = styles[2];
        var astyle2 = styles[3];
        var tablestyle = styles[4];
        var arrowstyle = styles[5];
        var headstyle = styles[6];
        var substyle = styles[7];
        var tdstyle = styles[8];
        
        var down = Number(state.Calendar.now.down);
        down = getdown(down);
        var moMenu = getMoMenu();
        var ordinal = state.Calendar.now.ordinal;
        
        var nowdate;
        
        nowdate = getDate(ordinal).split(',');
        
        var month = nowdate[0];
        var day = nowdate[1];
        var suffix = getsuffix(day);
        
        var start = state.Calendar.now.startdate.split(',');
        var startdate = start[0]+getsuffix(start[0])+' '+start[1]+', '+start[2];
        var timeselect = timemenu();
        
        sendChat('Calendar', '/w gm <div ' + divstyle + '>' + //--
            '<div ' + headstyle + '>Calendar</div>' + //--
            '<div ' + substyle + '>Menu (v.' + state.Calendar.now.version + ')</div>' + //--
            '<div ' + arrowstyle + '></div>' + //--
            '<table ' + tablestyle + '>' + //--
            '<tr><td ' + tdstyle + '>World: </td><td ' + tdstyle + '>Golarion</td></tr>' + //--
            '<tr><td ' + tdstyle + '>Date: </td><td ' + tdstyle + '>'+ month + ' ' + day + suffix + ', ' + state.Calendar.now.year + '</td></tr>' + //--
            '<tr><td ' + tdstyle + '>Days: </td><td ' + tdstyle + '><a ' + astyle1 + '"href="!setdown,?{Down Days}">' + down + '</a></td></tr>' + //--
            '<tr><td ' + tdstyle + '>Time: </td><td ' + tdstyle + '><a ' + astyle1 + '" href="!settime,'+ timeselect + '">' + state.Calendar.now.time + '</a></td></tr>' + //--
            '</table>' + //--
            'Weather: ' + state.Calendar.now.weather + //--
            '<br><br><div style="text-align:center;"><a ' + astyle2 + '" href="!addday,?{Days to add?|1}">Advance the Date</a></div>' + //--
            '<div style="text-align:center;"><a ' + astyle2 + '" href="!addtime,?{Advance how many hours?|0},?{Advance how many minutes?|0}">Advance the Time</a></div>' + //--
            '<div style="text-align:center;"><a ' + astyle2 + '" href="!weather,?{Weather|Roll|Edit,?{Edit Weather}}">Change Weather</a></div>' + //--
            '<div style="text-align:center;"><a ' + astyle2 + '" href="!eventlog,?{Include time?|Yes|No},?{Notes}">Log Day</a></div>' + //--
            '<div style="text-align:center;"><a ' + astyle2 + '" href="!playercal">Show to Players</a></div>' + //--
            '<div style="text-align:center;"><a ' + astyle2 + '" href="!settings">Settings</a></div>' + //--
            '</div>'
        );
    },
    
    calmenu = function() {
        
        var styles = calstyle().split("|");
        var colour = styles[0];
        var divstyle = styles[1];
        var astyle1 = styles[2];
        var astyle2 = styles[3];
        var tablestyle = styles[4];
        var arrowstyle = styles[5];
        var headstyle = styles[6];
        var substyle = styles[7];
        var tdstyle = styles[8];
        
        var down = Number(state.Calendar.now.down);
        down = getdown(down);
        var moMenu = getMoMenu();
        var ordinal = state.Calendar.now.ordinal;
        
        var nowdate;
        
        nowdate = getDate(ordinal).split(',');
        
        var month = nowdate[0];
        var day = nowdate[1];
        
        var start = state.Calendar.now.startdate.split(',');
        var startdate = start[0]+getsuffix(start[0])+' '+start[1]+', '+start[2];
        var timeselect = timemenu();
        
        switch (state.Calendar.now.useimage) {
            case 'OFF':
                var changeimage='ON,&#64;{target|Select Clock Token|token_id}';
                break;
            case 'ON':
                var changeimage='OFF';
                break;
        }
        
        sendChat('Calendar', '/w gm <div ' + divstyle + '>' + //--
            '<div ' + headstyle + '>Calendar</div>' + //--
            '<div ' + substyle + '>Menu (v.' + state.Calendar.now.version + ')</div>' + //--
            '<div ' + arrowstyle + '></div>' + //--
            '<table ' + tablestyle + '>' + //--
            '<tr><td ' + tdstyle + '>World: </td><td ' + tdstyle + '>Golarion</td></tr>' + //--
            '<tr><td ' + tdstyle + '>Start Date: </td><td ' + tdstyle + '><a ' + astyle1 + '" href="!startdate,?{Day},?{Month},?{Year}">' + startdate + '</a></td></tr>' + //--
            '<tr><td ' + tdstyle + '>Day: </td><td ' + tdstyle + '><a ' + astyle1 + '" href="!setday,?{Day?|1},' + month +'">' + day + '</a></td></tr>' + //--
            '<tr><td ' + tdstyle + '>Month: </td><td ' + tdstyle + '><a ' + astyle1 + '" href="!setmonth,' + day + moMenu + month + '</a></td></tr>' + //--
            '<tr><td ' + tdstyle + '>Year: </td><td ' + tdstyle + '><a ' + astyle1 + '" href="!setyear,?{Year?|1486}">' + state.Calendar.now.year + '</a></td></tr>' + //--
            '<tr><td ' + tdstyle + '>Time type: </td><td ' + tdstyle + '><a ' + astyle1 + '" href="!settimetype,?{Time?|OFF|24 Hour|General}">' + state.Calendar.now.timetype + '</a></td></tr>' + //-
            '<tr><td ' + tdstyle + '>Time: </td><td ' + tdstyle + '><a ' + astyle1 + '" href="!settime,'+ timeselect + '">' + state.Calendar.now.time + '</a></td></tr>' + //--
            '<tr><td ' + tdstyle + '>Down Day<br>Divider: </td><td ' + tdstyle + '><a ' + astyle1 + '" href="!setdiv,?{Down Day Divider?|1}">' + state.Calendar.now.divider + '</a></td></tr>' + //--
            '<tr><td ' + tdstyle + '>Use Images: </td><td ' + tdstyle + '><a ' + astyle1 + '" href="!useimage,' + changeimage + '">' + state.Calendar.now.useimage + '</a></td></tr>' + //--
            '</table>' + //--
            '<div style="text-align:center;"><a ' + astyle2 + '" href="!cal">Main Menu</a></div>' + //--
            '</div>'
        );
    },
    
    showcal = function(msg) {
        var nowdate;
        var ordinal = state.Calendar.now.ordinal;
        
        nowdate = getDate(ordinal).split(',');
        
        var month = nowdate[0];
        var day = nowdate[1];
        var down = state.Calendar.now.down;
            down = getdown(down);
        var suffix = getsuffix(day);
        var colour = '#7E2D40';
        var divstyle = 'style="width: 189px; border: 1px solid black; background-color: #ffffff; padding: 5px;"'
        var tablestyle = 'style="text-align:center;"';
        var arrowstyle = 'style="border: none; border-top: 3px solid transparent; border-bottom: 3px solid transparent; border-left: 195px solid ' + colour + '; margin-bottom: 2px; margin-top: 2px;"';
        var headstyle = 'style="color: ' + colour + '; font-size: 18px; text-align: left; font-variant: small-caps; font-family: Times, serif;"';
        var substyle = 'style="font-size: 11px; line-height: 13px; margin-top: -3px; font-style: italic;"';
        
        var timestr;
        var downstr;
        
        if(state.Calendar.now.time!="OFF"){
            timestr = '<br><b>The time is:</b> '+state.Calendar.now.time;
        }else{
            timestr = '';
        }
        
        if(down!=0){
            downstr = '<br>Players have ' + down + ' downtime days.';
        }else{
            downstr = '';
        }
        
        sendChat(msg.who, '<div ' + divstyle + '>' + //--
            '<div ' + headstyle + '>Calendar</div>' + //--
            '<div ' + substyle + '>Golarion</div>' + //--
            '<div ' + arrowstyle + '></div>' + //--
            '<b>' + month + ' ' + day + suffix + ', ' + state.Calendar.now.year + '</b>' + //--
            timestr + //--
            downstr + //--
            '<br><b>Today\'s weather:</b>' + state.Calendar.now.weather
        );
    },
    
    getdown = function(days) {
        var down = Number(days);
        var div = Number(state.Calendar.now.divider);
        
        if(div!=0){
            down = down/div;
        }
        
        return down;
    },
    
    getMoMenu = function() {
        var leap = checkLeap();
        var moMenu;
        
        return ',?{Month|Abadius|Calistril|Pharast|Gozran|Desnus|Sarenith|Erastus|Arodus|Rova|Lamashan|Neth|Kuthona}">';
    },
    
    checkLeap = function(){
        
        var leap;
        var remainder;
        var year = Number(state.Calendar.now.year);
        
        remainder = year % 8;
        
        if(remainder==0){
            leap = 1;
        }else{
            leap = 0;
        }
        
        return leap;
    },
    
    getDate = function(options){
        var day = Number(options);
        var date;
        var month;
        
        if(day>0 && day<=31){
            month="Abadius"; 
            date=day;
        }else if(day>31 && day<=59){
            month="Calistril"; 
            date=day-31;
        }else if(day>59 && day<=90){
            month="Pharast"; 
            date=day-59;
        }else if(day>90 && day<=120){
            month="Gozran";
            date=day-90;
        }else if(day>120 && day<=151){
            month="Desnus";
            date=day-120;
        }else if(day>151 && day<=181){
            month="Sarenith";
            date=day-151;
        }else if(day>181 && day<=212){
            month="Erastus";
            date=day-181;
        }else if(day>212 && day<=243){
            month="Arodus";
            date=day-212;
        }else if(day>243 && day<=273){
            month="Rova";
            date=day-243;
        }else if(day>273 && day<=304){
            month="Lamashan";
            date=day-273;
        }else if(day>304 && day<=334){
            month="Neth"
            date=day-304;
        }else if(day>334 && day<=365){
            month="Kuthona";
            date=day-334;
        }else{
            month="January";
            date='1';
        }
            
        var array=month+','+String(date);
        return array;    
    },
    
    getOrdinal = function(options){
        var args = options.content.split(",");
        var date = Number(args[1]);
        var month = args[2];
        var ordinal = state.Calendar.now.ordinal;
        
        switch(month) {
            case 'Abadius':
                ordinal = date;
                break;
            case 'Calistril':
                ordinal = 31+date;
                break;
            case 'Pharast':
                ordinal = 60+date;
                break;
            case 'Gozran':
                ordinal = 91+date;
                break;
            case 'Desnus':
                ordinal = 121+date;
                break;
            case 'Sarenith':
                ordinal = 152+date;
                break;
            case 'Erastus':
                ordinal = 182+date;
                break;
            case 'Arodus':
                ordinal = 213+date;
                break;
            case 'Rova':
                ordinal = 244+date;
                break;
            case 'Lamashan':
                ordinal = 274+date;
                break;
            case 'Neth':
                ordinal = 305+date;
                break;
            case 'Kuthona':
                ordinal = 366+date;
                break;
            }
        state.Calendar.now.ordinal = ordinal;
    },
    
    addday = function(no){
        var leap = checkLeap();
        var days = Number(no);
        var ordinal = Number(state.Calendar.now.ordinal);
        var div = state.Calendar.now.divider;
        
        if(div!=0){
            state.Calendar.now.down = Number(state.Calendar.now.down)+days;
        }
        
        var newordinal = ordinal+days;
        
        if(leap==0 && ordinal <= 60 && newordinal >= 60){
            state.Calendar.now.ordinal = newordinal+1;
        }else{
            state.Calendar.now.ordinal = newordinal;
        }
        
        if(newordinal>366){
            state.Calendar.now.ordinal=newordinal-366;
            state.Calendar.now.year = Number(state.Calendar.now.year)+1;
        }
    },
    
    addtime = function(addhours,addminutes){
        if (state.Calendar.now.timetype === "OFF" || state.Calendar.now.timetype === "General"){
            sendChat("Calendar","/w gm You must select the 24 hour clock to use the advance time function.");
            return;
        }
        
        var timecheck = state.Calendar.now.time;
        var currtime = state.Calendar.now.time.split(":");
        var hour = currtime[0];
        var minute = currtime[1];
        
        var newminute = Number(minute) + Number(addminutes);
        var minhour = 0;
        
        while (newminute >= 60) {
            
            newminute = newminute - 60;
            minhour ++;
            
        }
        
        if (newminute < 10) {
            newminute = '0' + newminute;
        }
        
        var newhour = Number(hour) + Number(addhours) + minhour;
        var day = 0;
        
        while (newhour >= 24) {
            
            newhour = newhour - 24;
            day ++;
            
        }
        
        if (newhour < 10) {
            newhour = '0' + newhour;
        }
        
        if (day > 0) {
            addday(day);
            weather();
        }
        
        state.Calendar.now.time = newhour + ":" + newminute;
        
        var clockface = (Number(newhour) >= 12) ? Number(newhour) - 12 : Number(newhour);
        setimage(state.Calendar.now.clockimage, clockface);
    },
    
    getCleanImagesrc = function (imgsrc) {
        var parts = imgsrc.match(/(.*\/images\/.*)(thumb|med|original|max)([^?]*)(\?[^?]+)?$/);
        if(parts) {
            return parts[1]+'thumb'+parts[3]+(parts[4]?parts[4]:`?${Math.round(Math.random()*9999999)}`);
        }
        return;
    },
    
    setimage = function(img, side){
        let o = getObj("graphic", img);
        let allSides = o.get("sides").split("|");
        let thisSide = o.get("currentSide") ;
        
        var nextURL = getCleanImagesrc(decodeURIComponent(allSides[side]));
        o.set({
            currentSide: side,
            imgsrc: nextURL
        });
    },
    
    
    getsuffix = function(day) {
        
        var date = day;
        var suffix;
        
        if (date == 'festival'){
            suffix = '';
        }else if (date == 1 || date == 21 ){
            suffix = 'st';
        }else if (date == 2 || date == 22){
            suffix = 'nd';
        }else if (date == 3 || date == 23){
            suffix = 'rd';
        }else{
            suffix = 'th';
        }
        
        return suffix;
    },
    
    weather = function() {
        var roll;
        var temperature;
        var wind;
        var precipitation;
        var season;
        var ordinal = state.Calendar.now.ordinal;
        
        if(ordinal > 349 || ordinal <= 75){
            season = 'Winter'
        }else if(ordinal <= 166){
            season = 'Spring'
        }else if(ordinal <=257 ){
            season = 'Summer'
        }else if(ordinal <=349 ){
            season = 'Fall'
        }
        
        roll = Math.floor(Math.random()*(20-1+1)+1);
        if(roll>=15 && roll<=17){
            switch(season) {
                case 'Winter':
                    temperature = 'It is a bitterly cold winter day. ';
                    break;
                case 'Spring':
                    temperature = 'It is a cold spring day. ';
                    break;
                case 'Summer':
                    temperature = 'It is a cool summer day. ';
                    break;
                case 'Fall':
                    temperature = 'It is a cold fall day. ';
                    break;
            }
        }else if(roll>=18 && roll<=20){
            switch(season) {
                case 'Winter':
                    temperature = 'It is a warm winter day. ';
                    break;
                case 'Spring':
                    temperature = 'It is a hot spring day. ';
                    break;
                case 'Summer':
                    temperature = 'It is a blisteringly hot summer day. ';
                    break;
                case 'Fall':
                    temperature = 'It is a hot fall day. ';
                    break;
            }
        }else{
            switch(season) {
                case 'Winter':
                    temperature = 'It is a cold winter day. ';
                    break;
                case 'Spring':
                    temperature = 'It is a mild spring day. ';
                    break;
                case 'Summer':
                    temperature = 'It is a hot summer day. ';
                    break;
                case 'Fall':
                    temperature = 'It is a mild fall day. ';
                    break;
            }
            
        }
        
        roll = Math.floor(Math.random()*(20-1+1)+1);
        if(roll>=15 && roll<=17){
            wind='There is a light breeze and ';
        }else if(roll>=18 && roll<=20){
            wind='There is a howling wind and ';
        }else{
            wind='The air is still and ';
        }
        
        roll = Math.floor(Math.random()*(20-1+1)+1);
        if(roll>=15 && roll<=17){
            precipitation="Light rain or snow.";
            if(season=='Winter'){
                precipitation = 'snow falls softly on the ground.';
            }else{
                precipitation = 'a light rain falls from the sky.';
            }
        }else if(roll>=18 && roll<=20){
            if(season=='Winter'){
                precipitation = 'snow falls thick and fast from the sky.';
            }else{
                precipitation = 'a torrential rain begins to fall.';
            }
        }else{
            roll = randomInteger(2);
            if(roll==1){
                precipitation = 'the sky is overcast.';
            }else{
                precipitation = 'the sky is clear.';
            }
        }
        
        var forecast=temperature+wind+precipitation;
        state.Calendar.now.weather = forecast;
    },

    timemenu = function() {
        var timetype = state.Calendar.now.timetype;
        var timeselect = 'OFF';
        
        switch(timetype) {
            case 'General':
                timeselect = '?{Time?|Morning (dawn)|Early Morning|Late Morning|Noon|Afternoon|Early Evening|Evening (dusk)|Late Evening|Midnight|After Midnight}';
                break;
            case '24 Hour':
                timeselect = '?{Hour|01|02|03|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24},?{Minute|00,:00|15,:15|30,:30|45,:45}';
                break;
        }
        
        return timeselect;
    },
    
    createHandout = function() {
    	var handout = createObj('handout',{
			name: handoutName
		});
        return handout;
	},

    getHandout = function() {
		var handout = filterObjs(function(o){
			return ( 'handout' === o.get('type') && handoutName === o.get('name') && false === o.get('archived'));
		})[0];
		
		if(handout) {
			return handout;
		} 
		
        return createHandout();
    },

    eventlog = function(msg) {
        var args = msg.content.split(",");
        var text = args[2];
        var addTime = args[1];
        var handout = getHandout();
        var nowdate;
        var ordinal = state.Calendar.now.ordinal;
        
        nowdate = getDate(ordinal).split(',');
        
        var month = nowdate[0];
        var day = nowdate[1];
        var suffix = getsuffix(day);
        var year = state.Calendar.now.year;
        var date = day + suffix + " " + month + ", " + year;
        
        handout.get('notes', function(nts) {
            
            if(!_.isNull(nts)){
                setTimeout(function(){
                    var newtext;
                    if (addTime === "Yes"){
                        newtext = nts + "<br>" + date + " " + state.Calendar.now.time + " - " + text;
                    } else {
                         newtext = nts + "<br>" + date + " - " + text;
                    }
                    
                    handout.set('notes', newtext);                      
                },0);
            }
            
            setTimeout(function(){},0);
        });
        
        sendChat('Calendar','/w gm A new note has been added to the Events Log handout.');
    },
    
    checkInstall = function() {
        if(typeof state.Calendar == "undefined"){
            setDefaults();
        }
        
        if ( state.Calendar.now.version != version ){
            checkDefaults();
        }
        
        log(`-=> GolarionCalendar v${version} <=-`);
    },
    
    registerEventHandlers = function() {
        on('chat:message', handleInput);
	};

	return {
	    CheckInstall: checkInstall,
		RegisterEventHandlers: registerEventHandlers
	};
	
}());

on("ready",function(){
	'use strict';
	Calendar.CheckInstall();
	Calendar.RegisterEventHandlers();
});

