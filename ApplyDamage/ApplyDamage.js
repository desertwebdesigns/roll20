/*
Modified for Basic saving throws in PF2E

 * Version 1.1.1
 * Original By Jakob Oesinghaus: https://gist.github.com/joesinghaus/9fd52108d07e5ef83ed879a7c6fdc68f
 * Changes in Version 1.1.1 and greater by Mark Stoecker
 * Roll20: https://app.roll20.net/users/580967/mark-s
 * BitBucket: https://bitbucket.org/desertwebdesigns/roll20/src/master/ApplyDamage/

*/
const ApplyDamage = (() => {
  "use strict";
  const version = "1.1.1",
    observers = {
      "change": [],
    },
    boundedBar = false,
    checkInstall = () => {
      log(`-=> ApplyDamage v${version} <=-`);
    },
    defaultOpts = {
      ids: "",
      saves: "",
      DC: "-1",
      dmg: "0",
      bar: "1"
    },
    getWhisperPrefix = (playerid) => {
      const player = getObj("player", playerid);
      if (player && player.get("_displayname")) {
        return `/w "${player.get("_displayname")}" `;
      }
      else {
        return "/w GM ";
      }
    },
    parseOpts = (content, hasValue) => {
      return content
        .replace(/<br\/>\n/g, " ")
        .replace(/({{(.*?)\s*}}\s*$)/g, "$2")
        .split(/\s+--/)
        .slice(1)
        .reduce((opts, arg) => {
          const kv = arg.split(/\s(.+)/);
          if (hasValue.includes(kv[0])) {
            opts[kv[0]] = (kv[1] || "");
          } else {
            opts[arg] = true;
          }
          return opts;
        }, {});
    },
    processInlinerolls = function (msg) {
      if (msg.inlinerolls && msg.inlinerolls.length) {
        return msg.inlinerolls.map(v => {
          const ti = v.results.rolls.filter(v2 => v2.table)
            .map(v2 => v2.results.map(v3 => v3.tableItem.name).join(", "))
            .join(", ");
          return (ti.length && ti) || v.results.total || 0;
        }).reduce((m, v, k) => m.replace(`$[[${k}]]`, v), msg.content);
      } else {
        return msg.content;
      }
    },
    processDamage = function(token, barCur, barMax, amt) {
      let newValue;
      if (boundedBar) {
        newValue = Math.min(Math.max(parseInt(token.get(barCur)) - amt, 0), parseInt(token.get(barMax)));
      } else {
        newValue = parseInt(token.get(barCur)) - amt;
      }
      return newValue;
    },
    handleError = (whisper, errorMsg) => {
      const output = `${whisper}<div style="border:1px solid black;background:#FFBABA;padding:3px">` +
        `<h4>Error</h4><p>${errorMsg}</p></div>`;
      sendChat("ApplyDamage", output);
    },
    finalApply = (results, dmg, bar) => {
      const barCur = `bar${bar}_value`,
        barMax = `bar${bar}_max`;
      Object.entries(results).forEach(([id, saved]) => {
        const token = getObj("graphic", id),
          prev = JSON.parse(JSON.stringify(token || {}));
        let newValue;
        if (token) {
            switch (saved) {
                case 'critsuccess':
                    newValue = processDamage(token, barCur, barMax, 0);
                    break;
                case 'success':
                    newValue = processDamage(token, barCur, barMax, Math.floor(dmg / 2));
                    break;
                case 'fail':
                    newValue = processDamage(token, barCur, barMax, dmg);
                    break;
                case 'critfail':
                    newValue = processDamage(token, barCur, barMax, dmg * 2);
                    break;
            }
        }
        if (!_.isUndefined(newValue)) {
          if (Number.isNaN(newValue)) newValue = token.get(barCur);
          token.set(barCur, newValue);
          notifyObservers("change", token, prev);
        }
      });
    },
    handleInput = (msg) => {
      if (msg.type === "api" && msg.content.search(/^!apply-damage\b/) !== -1) {
        const hasValue = ["ids", "saves", "DC", "dmg", "bar"],
          opts = Object.assign({}, defaultOpts, parseOpts(processInlinerolls(msg), hasValue));
        opts.ids = opts.ids.split(/,\s*/g);
        opts.saves = opts.saves.split(/,\s*/g);
        opts.DC = parseInt(opts.DC);
        opts.dmg = parseInt(opts.dmg);
        if (!playerIsGM(msg.playerid) && getObj("player", msg.playerid)) {
          handleError(getWhisperPrefix(msg.playerid), "Permission denied.");
          return;
        }
        if (!["1", "2", "3"].includes(opts.bar)) {
          handleError(getWhisperPrefix(msg.playerid), "Invalid bar.");
          return;
        }
        const results = _.reduce(opts.ids, function (m, id, k) {
          if (parseInt(opts.saves[k] || "0") >= opts.DC) {
            if (parseInt(opts.saves[k] || "0") >= opts.DC+10) {
                m[id] = "critsuccess";
            } else {
                m[id] = "success";
            }
          }
          else {
            if (parseInt(opts.saves[k]) <= opts.DC-10) {
                m[id] = "critfail";
            } else {
                m[id] = "fail";
            }
          }
          return m;
        }, {});
        finalApply(results, opts.dmg, opts.bar);
        const output = `${
          getWhisperPrefix(msg.playerid)
        }<div style="border:1px solid black;background:#FFF;padding:3px"><p>${
          (opts.dmg ? `<b>Crit Success Save:</b> 0 damage<br><b>Successful Save:</b> ${Math.floor(opts.dmg / 2)} damage<br><b>Failed Save:</b> ${opts.dmg} damage<br><b>Crit Failed Save:</b> ${opts.dmg * 2} damage` : "")}        </p></div>`;
        sendChat("ApplyDamage", output, null, { noarchive: true });
      }
      return;
    },
    notifyObservers = (event, obj, prev) => {
      observers[event].forEach(observer => observer(obj, prev));
    },
    registerObserver = (event, observer) => {
      if (observer && _.isFunction(observer) && observers.hasOwnProperty(event)) {
        observers[event].push(observer);
      } else {
        log("ApplyDamage event registration unsuccessful.");
      }
    },
    registerEventHandlers = () => {
      on("chat:message", handleInput);
    };

  return {
    checkInstall,
    registerEventHandlers,
    registerObserver
  };
})();

on("ready", () => {
  "use strict";
  ApplyDamage.checkInstall();
  ApplyDamage.registerEventHandlers();
  if ("undefined" !== typeof HealthColors) {
    ApplyDamage.registerObserver("change", HealthColors.Update);
  }
});
