/*
Turn Timer

Version     1.0
Author:     Mark Stoecker
Roll20:     https://app.roll20.net/users/580967/mark-s
BitBucket:  https://bitbucket.org/desertwebdesigns/roll20/src/master/TurnTimer/

Based on script By James Mowery:
GitHub:    https://github.com/JamesMowery/turn-timer
Contact:   https://app.roll20.net/users/1001679/james-mowery

endTurn function taken from TheAaron's TurnMarker script
GitHub:    https://github.com/shdwjk/Roll20API/tree/master/TurnMarker1
Contact:   https://app.roll20.net/users/104025/the-aaron
*/

/*
Future Version:
better defined alerts based on 50%, 25%, 30 seconds, and 15 seconds remaining time
User-defined options: 
    Combat Timer:
        send timer notifications to current player or all players
        automatically start combat timer when turn order is sorted
        automatically sort initiative and start combattimer
        default length (different than normal default timer length)
*/

var RLRGaming = RLRGaming || {};

RLRGaming.TurnTimer = RLRGaming.TurnTimer || (() => {
    'use strict';
    
    const version = "1.0";
    
    let seconds = 0;
    let timers = {"runcombat": 0};
    let image = state.RLRGaming.TurnTimer.config.defaultTimerImage;

    const createMessage = (msg) => {
        return "<div style='min-height: 32px; border:1px solid #CCC; \
        font-weight: bold;'><img src='" + image + "' \
        width='32px' height='32px' style='float: left;'> \
        <div style='min-height: 32px; vertical-align:middle; margin: 8px 0 0 42px;'>" + msg + "</div></div>"
    }
    
    const countDown = (timerName) => {
        timers[timerName].duration = timers[timerName].duration - 1;
        
        switch (timers[timerName].duration) {
            case 121:
                sendChat("TurnTimer", createMessage("2 Minutes Remaining on " + timerName + ((timers[timerName].iscombat) ? "<br>" + endTurnButton() : '')));
                break;
            case 61:
                sendChat("TurnTimer", createMessage("1 Minute Remaining on " + timerName + ((timers[timerName].iscombat) ? "<br>" + endTurnButton() : '')));
                break;
            case 31:
                sendChat("TurnTimer", createMessage("30 Seconds Remaining on " + timerName + ((timers[timerName].iscombat) ? "<br>" + endTurnButton() : '')));
                break;
            case 16:
                sendChat("TurnTimer", createMessage("15 Seconds Remaining on " + timerName + ((timers[timerName].iscombat) ? "<br>" + endTurnButton() : '')));
                break;
            case 0:
                sendChat("TurnTimer", createMessage("Time's Up - " + timerName));
                clearInterval(timers[timerName].timer);
                timers[timerName] = undefined;
                break;
        }
    }

    const getCharByToken = (tokenid) => {
        var token = getObj("graphic", tokenid);
        if (token === undefined)
            return false;
        else
            return getObj("character", token.get('represents'));
    };
    
    const endTurnButton = () => {
        return '<a style="margin: 5px 0px 8px; background-color: #f00; border-radius: 5px; box-shadow: 1px 1px 2px #000; border-left: none; border-top: none; color #000;" href="!tt --endturn">End Turn</a>';
    }

    const combatTimer = () => {
        if (timers.runcombat == 1) {
            if (timers.hasOwnProperty('combattimer') && 
                timers.combattimer.hasOwnProperty('timer')
            ) {
                clearInterval(timers.combattimer.timer);
            }
            
            var turnorder = JSON.parse(Campaign().get("turnorder"));
            var character = getCharByToken(turnorder[0].id)
            
            if(character.get("controlledby") !== "") {
                startTimer({
                    name: "combattimer",
                    duration: state.RLRGaming.TurnTimer.config.defaultTimerLength,
                    iscombat: 1
                });
            }
        }
    }
    
    const startTimer = (t) => {
        if (typeof timers[t.name] === 'object') {
                clearInterval(timers[t.name].timer);
            }
            
            var timerName = t.name || "timer " + Object.keys(timers).length;
            
            // Extracts the number of seconds in the command
            var seconds = Number(t.duration);
            
            timers[timerName] = {};
            
            // Begin the countdown
            timers[timerName].duration = seconds;
            timers[timerName].iscombat = t.iscombat
            timers[timerName].timer = setInterval(countDown, 1000, timerName);

            var timerMinutes = Math.floor(seconds / 60);
            var timerSeconds = seconds % 60;
            timerSeconds = (timerSeconds == 0) ? "00" : timerSeconds;
            
            var timerMessage = (timerMinutes > 0) ? timerMinutes + ":" + timerSeconds : timerSeconds + " seconds";
            
            if (t.iscombat == 1) {
                timerMessage = "Combat Timer Started for " + getCharByToken(JSON.parse(Campaign().get("turnorder"))[0].id).get("name") + " - " + timerMessage;
                timerMessage += "<br>" + endTurnButton();
            } else {
                timerMessage = "Timer " + timerName + " Started - " + timerMessage;
            }
            
            // Inform the players that the timer has started
           sendChat("TurnTimer", createMessage(timerMessage));
    }
    
    const stopTimer = (t) => {
        clearInterval(timers[t.name].timer);
        timers[t.name] = undefined;
        sendChat("TurnTimer", createMessage("Timer " + t.name + " Stopped"));
    }
    
    const endTurn = (playerid) => {
        var turnorder = JSON.parse(Campaign().get("turnorder")),
            current = getObj('graphic',_.first(turnorder).id),
            character = getCharByToken(_.first(turnorder).id);
            
            if(playerIsGM(playerid) ||
                ( current &&
                       ( _.contains(current.get('controlledby').split(','),playerid) ||
                       _.contains(current.get('controlledby').split(','),'all') )
                    ) ||
                ( character &&
                       ( _.contains(character.get('controlledby').split(','),playerid) ||
                       _.contains(character.get('controlledby').split(','),'all') )
                    )
            ) {
                Campaign().set("turnorder", JSON.stringify(turnorder.rotate(1)));
                // change:campaign:turnorder event is not fired by API script so we have to call the combatTimer() script manually
                combatTimer();
            }
    }
    
    const checkTimer = (t) => {
        var seconds = timers[t.name].duration;
        var timerMinutes = Math.floor(seconds / 60);
        var timerSeconds = seconds % 60;
        timerSeconds = (timerSeconds == 0) ? "00" : timerSeconds;
        
        var timerMessage = (timerMinutes > 0) ? timerMinutes + ":" + timerSeconds : timerSeconds + " seconds";
        
        sendChat("TurnTimer", createMessage(timerMessage + " remaining on timer " + t.name));
    }

    const handleInput = (msg) => {
        var timer = {
            start: 0,
            stop: 0,
            check: 0,
            name: '',
            iscombat: 0,
            duration: state.RLRGaming.TurnTimer.config.defaultTimerLength
        }
        
        if(msg.type == "api") {
            var args = msg.content.toLowerCase().split(/\s+--/);
            
            switch(args.shift()) {
                case '!tt':
                    _.each(args, (v,k) => {
                        var parts = v.split(/\s+/);
                        switch(parts.shift()) {
                            case 'name':
                                timer.name = parts.join(" ");
                                break;
                            case 'length':
                                timer.duration = Number(parts.join());
                                break;
                            case 'check':
                                timer.check = 1;
                                break;
                            case 'start':
                                timer.start = 1;
                                break;
                            case 'stop':
                                timer.stop = 1;
                                break;
                            case 'startcombat':
                                timers.runcombat = 1;
                                break;
                            case 'stopcombat':
                                timers.runcombat = 0;
                                timer.stop = 1;
                                timer.name = 'combattimer'
                                break;
                            case 'endturn':
                                endTurn(msg.playerid);
                                break;
                            /*case 'config':
                                config(parts);
                                break;*/
                            /* case 'all':
                                show all timers
                                break; */
                            /* case 'stopall'
                                stop all timers
                                break; */
                            /* case 'help':
                                help();
                                break; */
                            case 'log':
                                _.each(Object.keys(timers), (v,k) => {
                                    log(v + ": " + timers[v].duration);
                                });
                                break;
                        }
                    });
                    break;
            }

            // Process timer
            switch (true) {
                case (timer.start == 1 && timer.stop == 1):
                    sendChat("TurnTimer", createMessage("Error: You can not specify --start and --stop in the same command. Run <pre>!tt --help</pre> for more info"));
                    break;
                case (timer.check == 1 && (timer.stop != 1 && timer.start != 1)):
                    checkTimer(timer);
                    break;
                case (timer.start == 1):
                    startTimer(timer);
                    break;
                case (timer.stop == 1):
                    stopTimer(timer);
                    break;
            }
        }
    }
    
    const checkInstall = () => {
        if (!state.RLRGaming ||
            !state.RLRGaming.TurnTimer ||
            !state.RLRGaming.TurnTimer.version ||
            state.RLRGaming.TurnTimer.version !== version)
        {
            state.RLRGaming = state.RLRGaming || {};
            state.RLRGaming.TurnTimer= {
                version: version,
                gcUpdated: 0,
                config: {}
            };
        }
        checkGlobalConfig();
    }
    
    const checkGlobalConfig = () => {
        var gc = globalconfig && globalconfig.turntimer,
            st = state.RLRGaming.TurnTimer;
            
            
        if (gc && gc.lastsaved && gc.lastsaved > st.gcUpdated) {
            st.gcUpdated = gc.lastsaved;
            st.config.defaultTimerLength = gc['Default Timer Length'];
            st.config.defaultTimerImage = gc['Default Timer Image'];
        }
    }
    
    const registerEventHandlers = async () => {
        on('chat:message', handleInput);
        on('change:campaign:turnorder', combatTimer);
    }
    
    return {
        CheckInstall: checkInstall,
        RegisterEventHandlers: registerEventHandlers
    };
})();

on("ready", function() {
    'use strict';
    RLRGaming.TurnTimer.CheckInstall();
    RLRGaming.TurnTimer.RegisterEventHandlers();
});

Array.prototype.rotate = (function() {
    var unshift = Array.prototype.unshift,
        splice = Array.prototype.splice;

    return function(count) {
        var len = this.length >>> 0,
            count = count >> 0;

        unshift.apply(this, splice.call(this, count % len, len));
        return this;
    };
})();